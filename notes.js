const fs=require('fs')
const chalk=require('chalk')

function addNotes(title,body)
{
    var data=JSON.parse(fs.readFileSync('usersdata.json','utf-8'));
    var user={
        'id':(data.length)+1,
        'title':title,
        'body':body
    }
    data.push(user);
    fs.writeFileSync('usersdata.json',JSON.stringify(data))
    console.log("Data added successfully...")
}

function getNotes(title)
{
    var data=JSON.parse(fs.readFileSync('usersdata.json','utf-8'))
    var flag=false
    for(var i=0;i<data.length;i++)
    {
        if(data[i].title===title)
        {
            flag=true;
            console.log(chalk.blue.inverse("Title:-", data[i].title));
            console.log(chalk.blue.inverse("Body:-", data[i].body));
        }
    }
    if(flag==false)
    {
        console.log(chalk.yellow.inverse("No such note exists"));
    }
}

function deleteNote(title)
{
    const notes = JSON.parse(fs.readFileSync('usersdata.json','utf-8'))
    const noteToBeDeleted = notes.find(
      (note) =>
        note.title.toString().toLowerCase() === title.toString().toLowerCase()
    );
    if (!noteToBeDeleted) {
      console.log(chalk.yellow.inverse("No such note exists"));
    } else {
      notes.splice(notes.indexOf(noteToBeDeleted), 1);
      console.log(chalk.red.inverse("Note Deleted"));
    }
    fs.writeFileSync('usersdata.json',JSON.stringify(notes))
}

function listNotes()
{
    var data=JSON.parse(fs.readFileSync('usersdata.json','utf-8'))
    if(data.length==0)
    {
        console.log(chalk.red.inverse("Notes List Empty"));
    }
    else
    {
        console.log(chalk.green.inverse("My notes List"));
        for(var i=0;i<data.length;i++)
        {
            console.log(chalk.blue.inverse(data[i].id+"-"+data[i].title+"-"+data[i].body))
        }
    }
}

module.exports = { getNotes, addNotes, deleteNote, listNotes };