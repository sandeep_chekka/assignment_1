const readline = require('readline');
const chalk = require('chalk');

const notes = require('./notes');

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.question(
	`Select one:
1.Add a note
2.Delete a note
3.List all notes
4.Get a note by title\n`,
	userInput => {
		switch (userInput) {
			case '1':
				rl.question('Enter title of the note:- ', noteTitle => {
					rl.question(`Enter the note:- `, noteBody => {
						notes.addNotes(noteTitle, noteBody);
						rl.close();
					});
				});

				break;
			case '2':
				rl.question('Enter title of the note to be deleted:- ', noteTitle => {
					notes.deleteNote(noteTitle);
					rl.close();
				});
				break;
			case '3':
				notes.listNotes();
				rl.close();
				break;
			case '4':
				rl.question('Enter title of the note:- ', noteTitle => {
					notes.getNotes(noteTitle);
					rl.close();
				});
				break;
			default:
				console.log(chalk.red.inverse('Invalid Input'));
				break;
		}
	}
);